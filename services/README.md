# How to: deploy services

## Create an isolated user for deployment
The user running `make` will temporarily have access to the whole cluster. For that reason, it is recommended to use an isolated user account on your local system; the udev rules are required to use FIDO2/U2F tokens with SSH.
```bash
sudo useradd -M -s /usr/sbin/nologin -G plugdev -d /var/swarm-deployment swarm-deployment
sudo install -d -m 700 -o swarm-deployment /var/swarm-deployment
echo 'TAG=="security-device", GROUP="plugdev"' | sudo tee /etc/udev/rules.d/fido-plugdev.rules
```

Then, for all `make` commands, prefix them with this (creating an alias `swarm-make` in your `.bashrc` is recommended):
```bash
SSH_KEY="$(cat ~/.ssh/id_ecdsa_sk)" sudo --preserve-env=SSH_KEY -Hu swarm-deployment make
```

## Deploy or remove one or multiple services
```bash
swarm-make [service].up|down [...] clean
```

## Deploy everything as intended
```bash
swarm-make everything
```
