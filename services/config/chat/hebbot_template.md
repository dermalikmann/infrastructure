---
title: "#{{weeknumber}} This Month in den Softwerken"
author: {{author}}
date: {{today}}
tags: [{{projects}}]
categories: ["monthly-update"]
draft: false
---

Update on what happened across the X project in the week from {{timespan}}.

{{report}}

# That’s all for this week!

See you next week!
