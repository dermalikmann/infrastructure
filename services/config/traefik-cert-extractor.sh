#!/bin/sh
which jq >/dev/null || apk add --no-cache jq docker-cli
while true; do
  key=$(jq -r '.["'"$1"'"].Certificates[] | select(.domain.main == "'"$2"'") | .key' "/traefik-certs/$1.json" | base64 -d)
  cert=$(jq -r '.["'"$1"'"].Certificates[] | select(.domain.main == "'"$2"'") | .certificate' "/traefik-certs/$1.json" | base64 -d)
  if [ "$(cat /pem-certs/key.pem)" != "$key" ] || [ "$(cat /pem-certs/cert.pem)" != "$cert" ]; then
    echo "Certificate updated at $(date)"
    echo "$key" > "/pem-certs/key.pem" &&\
    echo "$cert" > "/pem-certs/cert.pem" &&\
    docker restart $(docker ps -qf "label=com.docker.swarm.service.name=$3")
  fi
  sleep 60
done
