#!/bin/sh
which envsubst >/dev/null || { apt-get update && apt-get -y install gettext-base; }
envsubst < /etc/homeserver-template.yaml > /data/homeserver.yaml
exec /start.py "$@"
