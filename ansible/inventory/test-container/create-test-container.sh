#!/bin/sh
if [ -z "$1" ]; then
	echo "Usage: $0 <containername>"
	exit 1
fi

set -e
printf "\033[1;34m(0/4) removing old container if necessary...\033[0m\n"
sudo podman rm --force "softwerke-$1" || true

printf "\033[1;34m(1/4) creating container...\033[0m\n"
sudo podman run --name "softwerke-$1" --hostname "$1.s.softwerke.localhost" --cap-add audit_write --cap-add audit_control --rm -it -d docker.io/rockylinux/rockylinux:8 /sbin/init

printf "\033[1;34m(2/4) updating /etc/hosts...\033[0m\n"
sudo sed -i '/\.s\.softwerke\.localhost$/d' /etc/hosts
echo "# resolving hostnames for all testing containers ending with .s.softwerke.localhost" | sudo tee --append /etc/hosts
sudo podman inspect -f '{{ .NetworkSettings.IPAddress }} {{ .Config.Hostname}}' $(sudo podman ps -q -f 'name=softwerke-*') | sudo tee --append /etc/hosts

printf "\033[1;34m(3/4) installing & configuring openssh-server...\033[0m\n"
sudo podman exec "softwerke-$1" dnf install -yq openssh-server
sudo podman exec "softwerke-$1" sh -c "sed -Ei '"'s/^root:[^:]*(:.*)$/root:\1/'"' /etc/shadow && sed -Ei -e '"'s/^#?(PermitEmptyPasswords)\s.*/\1 yes/'"' /etc/ssh/sshd_config && systemctl enable --now sshd"
ssh-keygen -f ~/.ssh/known_hosts -R "$1.s.softwerke.localhost"
ssh -oStrictHostKeyChecking=accept-new "root@$1.s.softwerke.localhost" echo "SSH connection is working!"

printf "\033[1;34m(4/4) all done!\033[0m\n"
echo "Connect to your container using the following command:"
echo "$ ssh root@$1.s.softwerke.localhost"
