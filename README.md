# Infrastruktur der Softwerke

## Server, DNS & mehr

Unsere Server laufen bei https://netcup.de. Das Passwort zum CCP kann im Passwort-Manager
nachgeschlagen werden; das SCP ist nur über das CCP erreichbar.

Domains, DNS und Speicherplatz (als Object Storage) laufen bei https://scaleway.com.

## CoreOS als Betriebssystem

CoreOS wurde gewählt, weil es sich selbst updated und kaum Wartung benötigt. Für Updates
ist ein Neustart notwendig, dafür wurde als Wartungszeitfenster 05:30 bis 06:25 gewählt.

Die Ignition-Konfiguration kann immer nur bei der (Neu)Installation von CoreOS verwendet
werden - kleinere Änderungen dürfen manuell erledigt werden, ansonsten ist eine
Neuinstallation meist empfehlenswert.

Für das Deployment von CoreOS ist eine `.env`-Datei notwendig (im `coreos`-Verzeichnis
dieses Repositories), deren Inhalt im Passwort-Manager zu finden ist.

Mehr Informationen zum Deployment neuer Server ist in der [coreos/README.md](https://codeberg.org/softwerke-magdeburg/infrastructure/src/branch/main/coreos)
zu finden.

## Docker Swarm + Docker Stack

Docker Swarm kann mehrere Systeme zu einem Cluster zusammenschließen, ist dabei aber
deutlich weniger flexibel als Kubernetes.

Docker Stack kann Projekte auf ein Docker-System oder einen Swarm-Cluster deployen, und
zwar mit den bekannten [Compose-Dateien](https://docs.docker.com/compose/compose-file/compose-versioning/).

Die Projekte sind im Ordner `services` zu finden und können mit `swarm-make <service>.up clean`
gestartet und mit `swarm-make <service>.down clean` gestoppt werden - dafür ist ein
erstmaliges Setup erforderlich, wie in der [services/README.md](https://codeberg.org/softwerke-magdeburg/infrastructure/src/branch/main/services)
beschrieben ist. Ebenfalls ist eine zweite `.env`-Datei erforderlich, die ebenfalls im
Passwort-Manager zu finden ist..

Zur Konfiguration der Dienste werden statt Volumes `configs` genutzt, da diese
automatisch im Cluster verteilt werden.
