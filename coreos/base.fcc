variant: fcos
version: 1.0.0
passwd:
  users:
    - name: core
      groups: [adm, wheel, sudo, systemd-journal, docker]
      # openssl passwd -6
      password_hash: '$CORE_PASSWORD'
      ssh_authorized_keys:
        - sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBNP6ZbJ2f6wGMv/uPImbFegMWEZLdQloNQXNHJastP+lUorv+OjtcRXocPMJAC/csXUCYm1/J9sDhJ/dDtZuez8AAAAEc3NoOg== Moritz
        - sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBJx2DXzile6jdbu2zeKcf/ErotW4kklUUon5y2zSWE6jmYakItUldPnD/JeItzgBwy/j2UfpQbPlWD76N83Q0swAAAAEc3NoOg== Bolli
        - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOAEiA6FYHGipliBEPgZWCKV2CK9/apLbKSAQAThdRkF Malik
storage:
  directories:
  - path: /var/dockersrv
  links:
    - path: /etc/localtime
      target: ../usr/share/zoneinfo/Europe/Berlin
    - path: /srv/docker
      target: /var/dockersrv
  files:
    - path: /var/home/core/.bashrc
      overwrite: true
      contents:
        inline: |
          # Original CoreOS .bashrc: load /etc/bashrc & add ~/.local/bin and ~/bin to $$PATH
          ! [ -f /etc/bashrc ] || . /etc/bashrc
          [[ "$$PATH" =~ "$$HOME/.local/bin:$$HOME/bin:" ]] || PATH="$$HOME/.local/bin:$$HOME/bin:$$PATH"
          export PATH

          # Use a colourful bash prompt
          [ $$(tput colors 2>/dev/null || echo 0) -le 1 ] || PS1='\[\e[1;38;5;244m\]\t \[\e[1;36m\]\u@\H \[\e[1;33m\]\w \[\e[1;36m\]\$$ \[\e[0m\]'

          # Workaround for https://github.com/moby/moby/issues/33673
          function docker() {
            if [ "$$1" = "service" ] && [ "$$2" = "logs" ]; then
              shift 2
              args=()
              follow=
              for arg in "$$@"; do
                if [ "$$arg" = "-f" ] || [ "$$arg" = "--follow" ]; then follow=1
                else args+=( "$$arg" )
                fi
              done
              service="$${args[-1]}"
              args=("$${args[@]::$${#args[@]}-1}")
              /usr/bin/docker service logs --timestamps "$${args[@]}" "$$service" | sort -k 1 || return "$$?"
              [ -z "$$follow" ] || /usr/bin/docker service logs --timestamps --follow "$${args[@]}" --tail 0 "$$service" || return "$$?"
            else
              /usr/bin/docker "$$@" || return "$$?"
            fi
          }

          alias htop='docker run --rm -it -v "/etc/htop:/root/.config/htop" --pid=host lwzm/htop'
    - path: /etc/ssh/sshd_config
      overwrite: true
      mode: 0644
      contents:
        inline: |
          HostKey /etc/ssh/ssh_host_rsa_key
          HostKey /etc/ssh/ssh_host_ecdsa_key
          HostKey /etc/ssh/ssh_host_ed25519_key
          AuthorizedKeysFile .ssh/authorized_keys .ssh/authorized_keys.d/ignition .ssh/authorized_keys.d/afterburn

          PermitRootLogin no
          X11Forwarding yes

          PubkeyAuthentication yes
          PasswordAuthentication no
          ChallengeResponseAuthentication no
          KerberosAuthentication no
          GSSAPIAuthentication no
          UsePAM yes

          AcceptEnv LANG LC_CTYPE LC_NUMERIC LC_TIME LC_COLLATE LC_MONETARY LC_MESSAGES
          AcceptEnv LC_PAPER LC_NAME LC_ADDRESS LC_TELEPHONE LC_MEASUREMENT
          AcceptEnv LC_IDENTIFICATION LC_ALL LANGUAGE
          AcceptEnv XMODIFIERS
          PrintMotd no
          Subsystem	sftp	/usr/libexec/openssh/sftp-server
    - path: /etc/selinux/config
      overwrite: true
      mode: 0664
      contents:
        inline: |
          SELINUX=pass:quotes[*permissive*]
          SELINUXTYPE=targeted
    - path: /etc/zincati/config.d/maintenance-window.toml
      mode: 0644
      contents:
        inline: |
          [updates]
          strategy = "periodic"

          [[updates.periodic.window]]
          days = [ "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ]
          start_time = "05:30"
          length_minutes = 55
    - path: /etc/sysconfig/nftables.conf
      overwrite: true
      mode: 0600
      contents:
        inline: |
          table inet filter {
            chain input {
              type filter hook input priority 0;

              iifname lo accept;
              ct state established,related accept;
              ct state invalid drop;

              # prevent ping floods
              ip6 nexthdr icmpv6 icmpv6 type echo-request limit rate 3/second accept;
              ip protocol icmp icmp type echo-request limit rate 3/second accept;

              # Accept SSH connection with rate limit
              tcp dport 22 limit rate 6/minute accept;

              # Accept connections between the swarm nodes inside the Cloud vLAN
              ip saddr { 10.0.0.0/16 } tcp dport 2377 accept;

              # Drop anything else from outside (internal traffic is not firewalled manually)
              iifname "ens*" drop;
            }
            chain forward {
              type filter hook forward priority 0;
            }
            chain output {
              type filter hook output priority 0;
            }
          }
    - path: /etc/sysconfig/docker
      overwrite: true
      mode: 0644
      contents:
        inline: |
          # /etc/sysconfig/docker
  
          # Modify these options if you want to change the way the docker daemon runs
          OPTIONS="--selinux-enabled \
            --log-driver=journald \
            --storage-driver=overlay2 \
            --default-ulimit nofile=1024:1024 \
            --init-path /usr/libexec/docker/docker-init \
            --userland-proxy-path /usr/libexec/docker/docker-proxy \
          "
          # TODO: IPv6 support - maybe use https://github.com/moby/libnetwork/pull/2572
systemd:
  units:
    - name: nftables.service
      enabled: true
  units:
    - name: docker.service
      enabled: true
