# How to: deploy a new server

1. Upload the latest [Fedora CoreOS ISO](https://getfedora.org/de/coreos/download?tab=metal_virtualized&stream=stable) to the cdrom images on in the server console:
   ```bash
   curl -o coreos.iso https://builds.coreos.fedoraproject.org/...
   scp -P 2222 coreos.iso USERNAME@IPADDRESS:/cdrom/
   ```
2. Start the server from the CoreOS image
3. Run the following commands in the server console to allow connecting via SSH:
   ```bash
   sudo passwd core
   sudo rm /etc/ssh/sshd_config.d/40-disable-passwords.conf
   sudo systemctl reload sshd
   ```
4. Run `make <hostname>.softwerke.md SWARM_MODE=initiator|manager|worker` in this directory (ensure that your `.env` file is correct!)
5. Power off the server & remove the disk image
6. Possibly debug errors using the [emergency shell](https://docs.fedoraproject.org/en-US/fedora-coreos/emergency-shell/)
